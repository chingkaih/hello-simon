/*
 * Copyright (c) 2015 Wentworth Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


// [HelloSimon_Server]
// Ver. 1.2.3	April.21th, 2015	[Kai]	=> Add testing code for simple button reaction.
// Ver. 1.2.1	April.17th, 2015	[Kai]
// Ver. 1.2	April.7th, 2015		[Kai]
// 
// Programmer: Ching-Kai Huang

var http = require("http");
var url = require('url');
var fs = require('fs');
var io = require('socket.io');

// http.createServer() will create an instance of http.Server, and the 
// contain function(request, response) will be assign to event:'request'.
//
// event:request, function(request,response)
// => request: instance of http.IncomingMessage
// => response: instance of http.ServerResponse
var server = http.createServer(function(request, response) {
	// Update Frequency (msec)
	var updateFrequency = 10;
	var path = url.parse(request.url).pathname;

	switch (path) {
		case '/':
			fs.readFile(__dirname + "/simon.html", function(error, data) {
				if (error){
					response.writeHead(404);
					response.write("opps this doesn't exist - 404");
					return;
				} else {
					response.writeHead(200, {"Content-Type": "text/html"});
					response.write(data, "utf8");
				}
				response.end();
			});
			break;

		case '/testing.html':
			fs.readFile(__dirname + "/testing.html", function(error, data) {
				if (error){
					response.writeHead(404);
					response.write("opps this doesn't exist - 404");
					return;
				} else {
					response.writeHead(200, {"Content-Type": "text/html"});
					response.write(data, "utf8");
				}
				response.end();
			});
			break;

		case '/css/simon.css':
			fs.readFile(__dirname + path, function(error, data) {
				if (error){
					response.writeHead(404);
					response.write("opps this doesn't exist - 404");
					return;
				} else {
					response.writeHead(200, {"Content-Type": "application/javascript"});
					response.write(data, "utf8");
				}
				response.end();
			});
			break;

		default:
			response.writeHead(404);
			response.write("opps this doesn't exist - 404");
			response.end();
			return;
			break;
	}
});

server.listen(process.env.PORT || 8001);
var serv_io = io.listen(server);

var pIndex;			// Index of playerList. Player under this index is player of this round.
var playerList;			// An array of client id(String).
var sIndex;			// Index of 'sequence'. Element under this index is the color round player try to answer.
var sequence;			// A sequence of all elements(colors) in a simon game.
var timeoutDef = 30;		// Default timeout sec.
var timeout;			// Variable to memorize how much time left.
var timeCounter;		// Interval of counting down time out.

// Reset all variables of simon game.
function init(){
	clearInterval(timeCounter);
	pIndex = 0;
	playerList = [];
	sIndex = 0;
	sequence = [];
	timeout = timeoutDef;
}

init();

// [Event] 'connection'
// 	=> Trigger when a connection builded from client.
// 	=> All codes in this block are individual for each client.
serv_io.sockets.on("connection", function(socket) {
	socket.emit("serverLog", "[Event] connection");

	var clientID = "";
	console.log("Event: connection");

	// Trigger when client disconnect.
	socket.on("disconnect", function(){
		socket.emit("serverLog", "[Event] disconnect");

		console.log("[Disconnect] " + clientID);
		youLose();
	});

	// Trigger when round player pressing a button.
	// 	1. reset timeout.
	// 	2. Tell everyone which button the round player is pressing.
	socket.on("btnPress", function(color){
		socket.emit("serverLog", "[Event] btnPress");

		console.log("btnPress");
		timeout = timeoutDef;
		socket.broadcast.emit('player_press', color);
	});

	// Trigger when round player released a button.
	// 	1. Let everyone know which button round player were released.
	// 	2. Identify is round player already finished answer all elements in sequence.
	// 		L Yes:	a. add color at the end of sequence.
	// 			b. tell everyone go to next round.
	// 			c. clear timeout counter.
	// 		L No:	a. Is the color round player pressed fit the element in sequence?
	// 			L Yes:	Wait next pressing.
	// 			L No:	Faild.
	socket.on("btnRelease", function(color){
		socket.emit("serverLog", "[Event] btnRelease(" + color + ")");

		console.log("btnRelease");
		socket.broadcast.emit('player_release', color);
		if(sIndex >= sequence.length){
			console.log("round_end");
			sequence[sIndex] = color;
			sIndex = 0;
			socket.broadcast.emit('next_round');
			socket.emit('next', true);

			clearInterval(timeCounter);
			nextPlayer();
		}else{
			if(sequence[sIndex] == color){
				socket.emit('next', false);
				sIndex++;
			}else{
				youLose();
			}
		}
	});

	// Send back all sequence to client (so client able replay it.)
	socket.on("replay_request", function(){
		socket.emit("serverLog", "[Event] replay_request");

		console.log("replay");
		socket.emit("replay", sequence);
	});

	// Trigger when client asking it's round player or not.
	socket.on("wait_assign", function(id){
		socket.emit("serverLog", "[Event] wait_assign(" + id + ")");

		console.log(">>" + playerList[pIndex]);
		if(playerList[pIndex] == id){
			// Assign as a round player.
			socket.emit('assign', 3);

			// Timeout timmer start.
			timeCounter = setInterval(function(){
				timeout--;
				if(timeout < 10){
					socket.emit("serMsg", timeout + " sec left");
				}

				if(timeout < 0){
					clearInterval(timeCounter);
					youLose();
				}
			}, 1000);
		}else{
			// Assign as a waiting player.
			socket.emit('assign', 4);
		}
	});

	// Trigger when player try to join the game.
	// 	1. Check the client id been used or not.
	// 		L Yes:	register success.
	// 		L No:	register fail.
	// 	2. Identify the game started or not. (by number of players.)
	// 		L 1 player: waiting for new player.
	// 		L 2 player: tell all players start the game.
	// 		L 3~ player: game already started. Add client to the end of player list.
	socket.on("register", function(id){
		socket.emit("serverLog", "[Event] register(" + id + ")");

		console.log("[register] playerList.length=" + playerList.length);
		if(!idUsed(id)){
			clientID = id;
			playerList[playerList.length] = id;
			socket.broadcast.emit("new_player", id);
			socket.emit('rtn_register', 0);
			if(playerList.length == 2){
				console.log("game_start");
				socket.emit('game_start');
				socket.broadcast.emit('game_start');
			}else if(playerList.length > 2){
				socket.emit('game_start');
			}
		}else{
			socket.emit("rtn_register", -1);
		}
	});

	// This client faild answer the simon game.
	// All players beside round player are winner.
	function youLose(){
		socket.emit("serverLog", "[Event] youLose()");

		socket.emit('lose');
		socket.broadcast.emit('win');
		init();
	}
	//-----------------------------------------------
	socket.on("lightTgreen", function(){
		socket.broadcast.emit("lightTgreen");
	});
	socket.on("lightTblue", function(){
		socket.broadcast.emit("lightTblue");
	});
	socket.on("lightTred", function(){
		socket.broadcast.emit("lightTred");
	});
	socket.on("lightTyellow", function(){
		socket.broadcast.emit("lightTyellow");
	});
	socket.on("offTgreen", function(){
		socket.broadcast.emit("offTgreen");
	});
	socket.on("offTblue", function(){
		socket.broadcast.emit("offTblue");
	});
	socket.on("offTred", function(){
		socket.broadcast.emit("offTred");
	});
	socket.on("offTyellow", function(){
		socket.broadcast.emit("offTyellow");
	});
	//-----------------------------------------------
});

// Change the index to next round player.
function nextPlayer(){
	var x = pIndex + 1;
	if(x >= playerList.length){
		pIndex = 0;
	}else{
		pIndex = x;
	}
}

// Clearify the id already been used or not.
function idUsed(id){
	for(var x=0; x< playerList.length; x++){
		if(id == playerList[x]){
			return true;
		}
	}
	return false;
}

